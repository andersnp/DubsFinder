using System;
using System.Windows.Input;

using DubsFinder.Helpers;
using DubsFinder.Services;

using Windows.ApplicationModel;
using System.Collections.Generic;
using Windows.Globalization;
using Windows.UI.Xaml;

namespace DubsFinder.ViewModels
{
    public class SettingsViewModel : Observable
    {
        // TODO WTS: Add other settings as necessary. For help see https://github.com/Microsoft/WindowsTemplateStudio/blob/master/docs/pages/settings.md
        private ElementTheme _elementTheme = ThemeSelectorService.Theme;

        public ElementTheme ElementTheme
        {
            get { return _elementTheme; }

            set { Set(ref _elementTheme, value); }
        }

        private string _appDescription;

        public string AppDescription
        {
            get { return _appDescription; }
            set { Set(ref _appDescription, value); }
        }

        public string CurrentLanguage
        {
            get => ApplicationLanguages.PrimaryLanguageOverride;
        }

        public IReadOnlyList<string> AppLanguages
        {
            get => ApplicationLanguages.ManifestLanguages;
        }

        private ICommand _switchThemeCommand;

        public ICommand SwitchThemeCommand
        {
            get
            {
                if (_switchThemeCommand == null)
                {
                    _switchThemeCommand = new RelayCommand<ElementTheme>(
                        async (param) =>
                        {
                            await ThemeSelectorService.SetThemeAsync(param);
                        });
                }

                return _switchThemeCommand;
            }
        }

        public SettingsViewModel()
        {
        }

        public void Initialize()
        {
            AppDescription = GetAppDescription();
        }

        private string GetAppDescription()
        {
            var package = Package.Current;
            var packageId = package.Id;
            var version = packageId.Version;

            return $"{package.DisplayName} - {version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
        }
    }
}
