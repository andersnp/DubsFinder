using DubsFinder.Helpers;
using DubsFinder.Models;
using Microsoft.Toolkit.Uwp.UI;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.System.Threading;
using Windows.Foundation;
using Windows.Storage.AccessCache;
using System.IO;
using Microsoft.Toolkit.Uwp.Helpers;
using DubsFinder.Models.Comparers;
using System.Collections;
using DubsFinder.Models.Filters;
using Windows.UI.Xaml.Controls;
using ThreadPool = Windows.System.Threading.ThreadPool;
using Windows.System;
using Windows.UI.Popups;

namespace DubsFinder.ViewModels
{
    public class MainViewModel : Observable
    {
        private static MainViewModel _Instance;

        public static MainViewModel Instance
        {
            get
            {
                if (_Instance == null) _Instance = new MainViewModel();
                return _Instance;
            }
        }

        public StorageFolder SelectedFolder { get; private set; }

        #region Commands

        public RelayCommand BrowseCommand { get; private set; }

        public RelayCommand StartCommand { get; private set; }

        public RelayCommand CancelCommand { get; private set; }

        public RelayCommand ViewTypeChangedCommand { get; private set; }

        public RelayCommand SortingChangedCommand { get; private set; }

        public RelayCommand FilterAddedCommand { get; private set; }

        public RelayCommand<ItemClickEventArgs> FilterRemovedCommand { get; private set; }

        public RelayCommand DeleteSelectedCommand { get; private set; }

        public RelayCommand<StorageFileWrapper> ShowInExplorerCommand { get; private set; }

        public RelayCommand<object> RescanObjectCommand { get; private set; }

        public RelayCommand ExpandAllCommand { get; private set; }

        #endregion Commands

        #region Properties

        private int _NumThreads = 1;

        public string NumThreads
        {
            get => _NumThreads.ToString();
            set
            {
                if (int.TryParse(value, out int result))
                {
                    if (result > 0) _NumThreads = result;
                }
            }
        }

        private uint _BufferSize = 100;

        public string BufferSize
        {
            get => _BufferSize.ToString();
            set
            {
                if (uint.TryParse(value, out uint result))
                {
                    _BufferSize = result;
                }
            }
        }

        private string _FolderPath;

        public string FolderPath
        {
            get => _FolderPath;
            set
            {
                if (_FolderPath != value)
                {
                    _FolderPath = value;
                    OnPropertyChanged(nameof(FolderPath));
                }
            }
        }

        private int _NumFoundFiles;

        private object NumFoundFilesLock = new object();

        public int NumFoundFiles
        {
            get => _NumFoundFiles;
            set
            {
                lock (NumFoundFilesLock)
                {
                    _NumFoundFiles = value;
                }
            }
        }

        private int _NumScannedFiles;

        private object NumScannedFilesLock = new object();

        public int NumScannedFiles
        {
            get => _NumScannedFiles;
            set
            {
                lock (NumScannedFilesLock)
                {
                    _NumScannedFiles = value;
                }
            }
        }

        private int _NumErrors;

        private object NumErrorsLock = new object();

        public int NumErrors
        {
            get => _NumErrors;
            set
            {
                lock (NumErrorsLock)
                {
                    _NumErrors = value;
                }
            }
        }

        public int ProcessedFiles
        {
            get => NumScannedFiles + NumErrors;
        }

        private CancellationTokenSource CancellationTokenSource { get; set; }

        private ConcurrentBag<StorageFolder> FoldersAwaitingScan { get; set; }

        private BlockingCollection<StorageFileWrapper> FoundFiles;

        private ConcurrentDictionary<Tuple<string, ulong>, ConcurrentDictionary<string, Bucket>> AnalysedFiles;

        private bool _IsFindingFiles = false;

        public bool IsFindingFiles
        {
            get => _IsFindingFiles;
            set
            {
                _IsFindingFiles = value;
                OnPropertyChanged(nameof(IsFindingFiles));
            }
        }

        private bool _IsAnalysersActive;

        public bool IsAnalysersActive
        {
            get => _IsAnalysersActive;
            set => _IsAnalysersActive = value;
        }

        private int NumWorkers = 0;
        private object NumWorkersLock = new object();

        private List<Bucket> Duplicates;

        public AdvancedCollectionView _DuplicatesCollectionView;

        public AdvancedCollectionView DuplicatesCollectionView
        {
            get => _DuplicatesCollectionView;
            set => Set(ref _DuplicatesCollectionView, value);
        }

        public List<Tuple<Bucket, StorageFileWrapper>> SelectedFiles { get; set; }

        public IList<Tuple<string, ResultsViewType>> ResultsViewTypes { get; private set; }

        public Tuple<string, ResultsViewType> SelectedResultsViewType { get; set; }

        public IList<Tuple<string, SortDescription>> SortingOptions { get; private set; }

        public Tuple<string, SortDescription> SelectedSortingOption { get; set; }

        public IList<Tuple<string, IFilter>> Filters { get; private set; }

        public Tuple<string, IFilter> SelectedFilter { get; set; }

        public ObservableCollection<Tuple<string, IFilter>> SelectedFilters { get; set; }

        private bool AllExpanded { get; set; }

        #endregion Properties

        private MainViewModel()
        {
            BrowseCommand = new RelayCommand(Browse);
            StartCommand = new RelayCommand(Start, CanStart);
            CancelCommand = new RelayCommand(CancelScan, CanCancelScan);
            DeleteSelectedCommand = new RelayCommand(DeleteSelected, CanDeleteSelected);
            ViewTypeChangedCommand = new RelayCommand(ResultsViewTypeChanged);
            SortingChangedCommand = new RelayCommand(SortingChanged);
            FilterAddedCommand = new RelayCommand(FilterAdded);
            FilterRemovedCommand = new RelayCommand<ItemClickEventArgs>(FilterRemoved);
            ShowInExplorerCommand = new RelayCommand<StorageFileWrapper>(ShowInExplorer);
            RescanObjectCommand = new RelayCommand<object>(RescanObject);
            SelectedFilters = new ObservableCollection<Tuple<string, IFilter>>();
            ExpandAllCommand = new RelayCommand(ExpandAll);
            AllExpanded = false;
            SetupViewTypes();
            SetupSortingOptions();
            SetupFilters();
        }

        private void SetupViewTypes()
        {
            ResultsViewTypes = new List<Tuple<string, ResultsViewType>>
            {
                new Tuple<string, ResultsViewType>(ResultsViewType.Grid.ToString(), ResultsViewType.Grid),
                new Tuple<string, ResultsViewType>(ResultsViewType.List.ToString(), ResultsViewType.List),
            };
            SelectedResultsViewType = ResultsViewTypes[0];
        }

        private void SetupSortingOptions()
        {
            SortingOptions = new List<Tuple<string, SortDescription>>
            {
                new Tuple<string, SortDescription>("A - Z", new SortDescription(SortDirection.Ascending, new NameComparer())),
                new Tuple<string, SortDescription>("Z - A", new SortDescription(SortDirection.Descending, new NameComparer())),
                new Tuple<string, SortDescription>("Small - Big", new SortDescription(SortDirection.Ascending, new SizeComparer())),
                new Tuple<string, SortDescription>("Big - Small", new SortDescription(SortDirection.Descending, new SizeComparer())),
            };
        }

        private void SetupFilters()
        {
            Filters = new List<Tuple<string, IFilter>>
            {
                new Tuple<string, IFilter>("Audio", new AudioFilter()),
                new Tuple<string, IFilter>("Images", new ImagesFilter()),
                new Tuple<string, IFilter>("Videos", new VideosFilter()),
                new Tuple<string, IFilter>("Others", new OthersFilter()),
            };
        }

        #region MainPage

        private void Start()
        {
            Views.Shell.Current.NavigateToScanning(SelectedFolder);
        }

        private bool CanStart()
        {
            return SelectedFolder != null;
        }

        private async void Browse()
        {
            var picker = new FolderPicker()
            {
                SuggestedStartLocation = PickerLocationId.ComputerFolder
            };
            picker.FileTypeFilter.Add("*");

            var folder = await picker.PickSingleFolderAsync();
            if (folder != null)
            {
                FolderPath = folder.Path;
                SelectedFolder = folder;
                StorageApplicationPermissions.FutureAccessList.AddOrReplace("rootFolder", folder);
                StartCommand.OnCanExecuteChanged();
            }
        }

        #endregion MainPage

        #region ScanningPage

        internal async void StartScan(object folderParameter)
        {
            NumErrors = 0;
            NumFoundFiles = 0;
            NumScannedFiles = 0;
            CancellationTokenSource = new CancellationTokenSource();
            FoldersAwaitingScan = new ConcurrentBag<StorageFolder>();
            FoundFiles = new BlockingCollection<StorageFileWrapper>();
            AnalysedFiles = new ConcurrentDictionary<Tuple<string, ulong>, ConcurrentDictionary<string, Bucket>>();
            CancelCommand.OnCanExecuteChanged();
            var token = CancellationTokenSource.Token;

            if (folderParameter is StorageFolder folder)
            {
                IsFindingFiles = true;
                // Set up a thread to periodically update the UI
                var period = TimeSpan.FromMilliseconds(20);
                var uiUpdater = ThreadPoolTimer.CreatePeriodicTimer(async (handle) =>
                {
                    await TriggerUIUpdateAsync();

                    if (!IsFindingFiles && !IsAnalysersActive && !token.IsCancellationRequested)
                    {
                        handle.Cancel();
                    }
                }, period);

                var worker = ThreadPool.RunAsync(handler =>
                {
                    Debug.WriteLine("Starting worker 1");
                    while (true)
                    {
                        Task.Run(async () =>
                        {
                            var maxItem = 100U;
                            var current = 0U;
                            var files = await folder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.OrderByName, current, maxItem);
                            while (files.Count > 0 && !token.IsCancellationRequested)
                            {
                                var filesCopy = files.ToArray();
                                current += (uint)files.Count;
                                var task = Task.Run(async () =>
                                {
                                    NumFoundFiles += files.Count;
                                    foreach (var file in filesCopy) FoundFiles.Add(await StorageFileWrapper.CreateStorageFileWrapperAsync(file));
                                });
                                files = await folder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.OrderByName, current, maxItem);
                                await task;
                                filesCopy = null;
                            }
                        }).Wait();
                        {
                            IsAnalysersActive = true;
                            DispatcherHelper.ExecuteOnUIThreadAsync(() => IsFindingFiles = false);
                            break;
                        }
                    }
                }, WorkItemPriority.High).AsTask();

                worker = worker.ContinueWith((handler) => { Worker(token); }, token);

                for (var i = 1; i < _NumThreads; i++)
                {
                    var j = i;
                    await ThreadPool.RunAsync((handler) =>
                    {
                        Debug.WriteLine($"Starting worker {j}");
                        Worker(token);
                    }, WorkItemPriority.Low);
                }
            }
            else
            {
                throw new ArgumentException($"Catastrophic error: Parameter not a StorageFolder but {folderParameter.GetType()}");
            }
        }

        private async void Worker(CancellationToken token)
        {
            Debug.WriteLine("Worker started");
            var hasher = new Hasher((_BufferSize * 1048576U) / (uint)_NumThreads);
            IsAnalysersActive = true;

            lock (NumWorkersLock) NumWorkers++;

            while (token.IsCancellationRequested != true)
            {
                if (FoundFiles.TryTake(out StorageFileWrapper file, 200))
                {
                    try
                    {
                        await AnalyseFileAsync(file, hasher);
                        NumScannedFiles++;
                    }
                    catch (Exception e)
                    {
                        NumErrors++;
                        Debug.WriteLine(e);
                    }
                    continue;
                }
                else if (!IsFindingFiles)
                {
                    // Release hasher (and its buffer)
                    hasher = null;
                    lock (NumWorkersLock)
                    {
                        NumWorkers--;
                        if (NumWorkers == 0)
                        {
                            IsAnalysersActive = false;
                            DispatcherHelper.ExecuteOnUIThreadAsync(() => Views.Shell.Current.NavigateToResults());
                        }
                    }
                    break;
                }
            }
            GC.Collect();
            Debug.WriteLine("Worker stopped");
        }

        private async Task AnalyseFileAsync(StorageFileWrapper file, Hasher hasher)
        {
            Debug.WriteLine($"Scanning file {file.DisplayName}");
            var bucket = new Bucket(file);
            var dictionary = new ConcurrentDictionary<string, Bucket>();
            dictionary.GetOrAdd("", bucket);

            var foundDict = AnalysedFiles.GetOrAdd(new Tuple<string, ulong>(file.ContentType, file.Size), dictionary);
            if (foundDict != dictionary)
            {
                lock (foundDict)
                {
                    if (foundDict.Count == 1 && foundDict.First().Key == "")
                    {
                        // Hash both values and redefine first value
                        var already = foundDict.First().Value;
                        Task.Run(async () => already.Hash = await hasher.HashFileAsync(await already.Files[0].AsStorageFileAsync())).Wait();
                        foundDict.TryRemove("", out Bucket throwAway);
                        foundDict.TryAdd(already.Hash, already);
                    }
                }
                var newHash = await hasher.HashFileAsync(await file.AsStorageFileAsync());
                bucket.Hash = newHash;
                if (foundDict.TryGetValue(newHash, out Bucket buck))
                {
                    buck.AddFile(file);
                }
                else
                {
                    Bucket b = foundDict.GetOrAdd(newHash, bucket);
                    if (b != bucket)
                    {
                        b.AddFile(file);
                    }
                }
            }
        }

        private async Task TriggerUIUpdateAsync()
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                OnPropertyChanged(nameof(NumFoundFiles));
                OnPropertyChanged(nameof(NumScannedFiles));
                OnPropertyChanged(nameof(NumErrors));
                OnPropertyChanged(nameof(ProcessedFiles));
                OnPropertyChanged(nameof(IsAnalysersActive));
            });
        }

        private void CancelScan()
        {
            CancellationTokenSource?.Cancel();
            CancelCommand.OnCanExecuteChanged();
            Views.Shell.Current.NavigateToResults();
        }

        private bool CanCancelScan()
        {
            return CancellationTokenSource?.IsCancellationRequested == false;
        }

        #endregion ScanningPage

        #region ResultsPage

        public void OrganiseDuplicates()
        {
            // Check if AnalysedFiles is set. It will only be null, if we _return_ to results page, not navigate from Scanning
            if (AnalysedFiles != null)
            {
                Duplicates = new List<Bucket>();
                DuplicatesCollectionView = new AdvancedCollectionView(Duplicates);
                DuplicatesCollectionView.SortDescriptions.Add(new SortDescription(SortDirection.Ascending, new NameComparer()));

                SelectedSortingOption = null;
                SelectedFilter = null;
                SelectedFilters.Clear();

                using (DuplicatesCollectionView.DeferRefresh())
                {
                    foreach (var item in AnalysedFiles.Values)
                    {
                        // Skip files with no match
                        if (item.Count == 1 && item.First().Key == "") continue;

                        foreach (var i in item.Values)
                        {
                            if (i.Files.Count > 1)
                            {
                                Duplicates.Add(i);
                            }
                        }
                    }
                }
                SelectedFiles = new List<Tuple<Bucket, StorageFileWrapper>>();
                AnalysedFiles = null;
                OnPropertyChanged(nameof(SelectedFiles));
            }
        }

        public void ExpandAll()
        {
            foreach (var bucket in Duplicates)
            {
                bucket.IsExpanded = !AllExpanded;
            }
            AllExpanded = !AllExpanded;
        }

        private void ResultsViewTypeChanged()
        {
            Debug.WriteLine($"View type changed to {SelectedResultsViewType}");
            foreach (var bucket in Duplicates)
            {
                bucket.ResultsViewType = SelectedResultsViewType.Item2;
            }
        }

        public void FileSelected(StorageFileWrapper file, Bucket bucket)
        {
            if (file == null || bucket == null) return;
            SelectedFiles.Add(new Tuple<Bucket, StorageFileWrapper>(bucket, file));
            OnPropertyChanged(nameof(SelectedFiles));
            DeleteSelectedCommand.OnCanExecuteChanged();
        }

        public void FileDeselected(StorageFileWrapper file, Bucket bucket)
        {
            if (file == null || bucket == null) return;
            SelectedFiles.Remove(new Tuple<Bucket, StorageFileWrapper>(bucket, file));
            OnPropertyChanged(nameof(SelectedFiles));
            DeleteSelectedCommand.OnCanExecuteChanged();
        }

        private void DeleteSelected()
        {
            var selectedFiles = SelectedFiles.ToArray();
            var duplicantsCollection = Duplicates.ToArray();
            using (DuplicatesCollectionView.DeferRefresh())
            {
                foreach (var item in selectedFiles)
                {
                    var bucketRemained = true;
                    foreach (var bucket in duplicantsCollection)
                    {
                        if (item.Item1 == bucket)
                        {
                            Debug.WriteLine($"Deleting {item.Item2.Path}");

                            try
                            {
                                bucket.RemoveFile(item.Item2);
                            }
                            catch (ArgumentOutOfRangeException e) { Debug.WriteLine(e); }
                            Task.Run(async () =>
                            {
                                try
                                {
                                    return await StorageFile.GetFileFromPathAsync(item.Item2.Path);
                                }
                                catch (FileNotFoundException)
                                {
                                    return null;
                                }
                            }).Result?.DeleteAsync();

                            if (bucket.Files.Count <= 1)
                            {
                                bucketRemained = false;
                                Duplicates.Remove(bucket);
                            }
                            break;
                        }
                    }
                    if (bucketRemained) OnPropertyChanged(nameof(Bucket.Files));
                }
            }
        }

        private bool CanDeleteSelected()
        {
            return SelectedFiles?.Count > 0;
        }

        private void SortingChanged()
        {
            Debug.WriteLine($"New sorting {SelectedSortingOption.Item1}");
            DuplicatesCollectionView.SortDescriptions[0] = SelectedSortingOption.Item2;
            DuplicatesCollectionView.Refresh();
        }

        private void FilterAdded()
        {
            if (SelectedFilter != null && !SelectedFilters.Contains(SelectedFilter))
            {
                SelectedFilters.Add(SelectedFilter);
                RefilterCollection();
            }
            SelectedFilter = null;
        }

        private void FilterRemoved(ItemClickEventArgs args)
        {
            SelectedFilters.Remove((Tuple<string, IFilter>)args.ClickedItem);
            RefilterCollection();
        }

        private void RefilterCollection()
        {
            DuplicatesCollectionView.Filter = new Predicate<object>((obj) =>
            {
                if (obj is Bucket b)
                {
                    if (SelectedFilters.Count == 0) return true;
                    else
                    {
                        return SelectedFilters.Any(tuple => tuple.Item2.Filter(b));
                    }
                }
                else return false;
            });
            DuplicatesCollectionView.Refresh();
        }

        private async void ShowInExplorer(StorageFileWrapper fileWrapper)
        {
            var options = new FolderLauncherOptions();
            try
            {
                var file = await fileWrapper.AsStorageFileAsync();
                options.ItemsToSelect.Add(file);

                await Launcher.LaunchFolderAsync(await file.GetParentAsync(), options);
            }
            catch (FileNotFoundException)
            {
                var resourceLoader = Windows.ApplicationModel.Resources.ResourceLoader.GetForCurrentView();
                var dialog = new MessageDialog(resourceLoader.GetString("Alert_FileNotFound"), resourceLoader.GetString("Alert_UnableToShowFile"));
                await dialog.ShowAsync();
            }
        }

        private async void RescanObject(object obj)
        {
            Debug.WriteLine(obj.GetType());

            if(obj is StorageFileWrapper fileWrapper)
            {
                try
                {
                    var file = await fileWrapper.AsStorageFileAsync();

                    // TODO: Rehash, and try to match to a new bucket
                }
                catch (FileNotFoundException)
                {
                    foreach(var bucket in Duplicates)
                    {
                        if(bucket.Files.Contains(fileWrapper))
                        {
                            bucket.RemoveFile(fileWrapper);

                            if (bucket.Files.Count <= 1)
                            {
                                DuplicatesCollectionView.Remove(bucket);
                                DuplicatesCollectionView.Refresh();
                            }
                            break;
                        }
                    }
                }
            }
            else if (obj is Bucket bucket)
            {
                foreach(var fw in bucket.Files.ToArray())
                {
                    try
                    {
                        var file = await fw.AsStorageFileAsync();

                        // TODO: Rehash, and try to match to a new bucket
                    }
                    catch (FileNotFoundException)
                    {
                        bucket.RemoveFile(fw);
                    }
                }

                if(bucket.Files.Count <= 1)
                {
                    DuplicatesCollectionView.Remove(bucket);
                    DuplicatesCollectionView.Refresh();
                }
            }
        }

        #endregion ResultsPage
    }

    public enum ResultsViewType
    {
        Grid,
        List,
    }
}
