﻿using DubsFinder.Models;
using DubsFinder.ViewModels;
using Microsoft.Toolkit.Uwp.Helpers;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DubsFinder.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ResultsPage : Page
    {
        private MainViewModel MainViewModel { get; } = MainViewModel.Instance;

        //private ResultsViewType ResultsViewType { get => MainViewModel.ResultsViewType; }

        public ResultsPage()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var loader = ResourceLoader.GetForCurrentView();
            Shell.Current.SetTitle(loader.GetString("Results_Title/Text"));
            MainViewModel.OrganiseDuplicates();
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException -= UnhandledException;
        }

        private async void UnhandledException(object sender, System.UnhandledExceptionEventArgs e)
        {
            var popup = new ContentDialog
            {
                Title = "Unknown error",
                Content = "An unknown error occurred in the background.",
                CloseButtonText = "OK",
            };

            await DispatcherHelper.ExecuteOnUIThreadAsync(async () => await popup.ShowAsync());
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }
    }
}
