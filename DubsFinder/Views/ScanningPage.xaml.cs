﻿using DubsFinder.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DubsFinder.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ScanningPage : Page
    {
        private MainViewModel MainViewModel { get; } = MainViewModel.Instance;
        private object Folder { get; set; }
        private NavigationMode NavigationMode { get; set; }

        public ScanningPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            NavigationMode = e.NavigationMode;
            Folder = e.Parameter;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            if (e.NavigationMode == NavigationMode.Back) MainViewModel.CancelCommand.Execute(null);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var loader = ResourceLoader.GetForCurrentView();
            Shell.Current.SetTitle(loader.GetString("Scanning_Title/Text"));
            switch (NavigationMode)
            {
                case NavigationMode.New:
                    MainViewModel.StartScan(Folder);
                    break;

                case NavigationMode.Back:
                    Frame.GoBack();
                    break;
            }
        }
    }

    internal class RadialProgressVisibityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if ((value as bool?) == true && !MainViewModel.Instance.IsFindingFiles) return Visibility.Visible;
            else return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
