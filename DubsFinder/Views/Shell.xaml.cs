using System;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Hosting;
using Windows.UI;

namespace DubsFinder.Views
{
    public sealed partial class Shell : Page
    {
        public static Shell Current { get; private set; }

        private Type CurrentPage { get; set; }

        public Shell()
        {
            InitializeComponent();

            Current = this;

            NavigationFrame.Navigate(typeof(MainPage));
            NavigationFrame.Navigated += NavigationFrame_Navigated;
            CurrentPage = typeof(MainPage);

            Window.Current.SetTitleBar(AppTitleBar);

            // Hide default title bar.
            var coreTitleBar = CoreApplication.GetCurrentView().TitleBar;

            // Register a handler for when the size of the overlaid caption control changes.
            // For example, when the app moves to a screen with a different DPI.
            coreTitleBar.LayoutMetricsChanged += CoreTitleBar_LayoutMetricsChanged;

            // Register a handler for when the title bar visibility changes.
            // For example, when the title bar is invoked in full screen mode.
            coreTitleBar.IsVisibleChanged += CoreTitleBar_IsVisibleChanged;
        }

        public bool NavigateToScanning(object folder)
        {
            if (CurrentPage != typeof(ScanningPage))
            {
                return NavigationFrame.Navigate(typeof(ScanningPage), folder);
            }
            else return false;
        }

        public bool NavigateToResults()
        {
            if (CurrentPage != typeof(ResultsPage))
            {
                return NavigationFrame.Navigate(typeof(ResultsPage));
            }
            else return false;
        }

        internal void SetTitle(string title)
        {
            TitlePage.Text = title;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (e.Handled) return;

            if (NavigationFrame.CanGoBack)
            {
                e.Handled = true;
                NavigationFrame.GoBack();
            }
        }

        private void NavigationFrame_Navigated(object sender, NavigationEventArgs e)
        {
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                NavigationFrame.CanGoBack == true ? AppViewBackButtonVisibility.Visible : AppViewBackButtonVisibility.Collapsed;
            CurrentPage = e.SourcePageType;
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationFrame.Navigate(typeof(SettingsPage));
        }

        private void CoreTitleBar_LayoutMetricsChanged(CoreApplicationViewTitleBar sender, object args)
        {
            UpdateTitleBarLayout(sender);
        }

        private void CoreTitleBar_IsVisibleChanged(CoreApplicationViewTitleBar sender, object args)
        {
            if (sender.IsVisible)
            {
                AppTitleBar.Visibility = Visibility.Visible;
            }
            else
            {
                AppTitleBar.Visibility = Visibility.Collapsed;
            }
        }

        private void UpdateTitleBarLayout(CoreApplicationViewTitleBar coreTitleBar)
        {
            // Get the size of the caption controls area and back button
            // (returned in logical pixels), and move your content around as necessary.
            LeftPaddingColumn.Width = new GridLength(coreTitleBar.SystemOverlayLeftInset);
            RightPaddingColumn.Width = new GridLength(coreTitleBar.SystemOverlayRightInset);

            // Update title bar control size as needed to account for system size changes.
            AppTitleBar.Height = coreTitleBar.Height;
        }
    }

    internal class SettingsVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value.GetType() == typeof(MainPage) || value.GetType() == typeof(ResultsPage)) return Visibility.Visible;
            else return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
