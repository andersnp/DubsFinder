using System;
using DubsFinder.ViewModels;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Text;
using System.IO;
using Windows.Storage;

namespace DubsFinder.Views
{
    public sealed partial class SettingsPage : Page
    {
        public SettingsViewModel ViewModel { get; } = new SettingsViewModel();
        // TODO WTS: Setup your privacy web in your Resource File, currently set to https://YourPrivacyUrlGoesHere

        public SettingsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModel.Initialize();
        }

        private void Page_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var loader = ResourceLoader.GetForCurrentView();
            Shell.Current.SetTitle(loader.GetString("Settings_Title/Text"));
        }

        private async void ThirdPartyNoticesButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Notice.Text))
            {
                var stringBuilder = new StringBuilder();
                var appInstalledFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                var licenseFolder = await appInstalledFolder.GetFolderAsync("Licenses");
                var files = await licenseFolder.GetFilesAsync();
                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];
                    var name = file.DisplayName.Replace("-license", "");
                    stringBuilder.AppendLine($"#{name}");
                    stringBuilder.AppendLine(await FileIO.ReadTextAsync(file));
                    if (i < files.Count - 1) stringBuilder.AppendLine("\n---");
                }

                Notice.Text = stringBuilder.ToString();
            }
            LicenseFlyout.ShowAt(this);
        }

        private void FlyoutCloseButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            LicenseFlyout.Hide();
        }
    }
}
