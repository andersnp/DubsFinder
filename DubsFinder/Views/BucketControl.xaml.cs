﻿using DubsFinder.Models;
using DubsFinder.ViewModels;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace DubsFinder.Views
{
    public sealed partial class BucketControl : UserControl
    {
        private MainViewModel MainViewModel { get; } = MainViewModel.Instance;
        public Bucket Bucket
        {
            get => (Bucket)GetValue(BucketProperty);
            set => SetValue(BucketProperty, value);
        }

        public static readonly DependencyProperty BucketProperty =
            DependencyProperty.Register(nameof(Bucket), typeof(Bucket), typeof(BucketControl), new PropertyMetadata(null));

        public BucketControl()
        {
            this.InitializeComponent();
        }

        private static FrameworkElement FindParentOfType(object sender, Type type)
        {
            FrameworkElement element = sender as FrameworkElement;
            while (element != null && !(element.GetType() == type))
            {
                element = VisualTreeHelper.GetParent(element) as FrameworkElement;
            }

            return element;
        }

        private void FilesGridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = e?.AddedItems;
            var deselected = e?.RemovedItems;

            foreach (var file in selected) FilesListView.SelectedItems.Add(file);
            foreach (var file in deselected) FilesListView.SelectedItems.Remove(file);

            if (selected != null)
            {
                foreach (var i in selected)
                {
                    Debug.WriteLine($"Selected items {i}");
                    if (i is StorageFileWrapper file)
                    {
                        var element = FindParentOfType(sender, typeof(Expander));
                        MainViewModel.FileSelected(file, element?.DataContext as Bucket);
                    }
                }
            }

            if (deselected != null)
            {
                foreach (var i in deselected)
                {
                    Debug.WriteLine($"Deselected items {i}");
                    if (i is StorageFileWrapper file)
                    {
                        var element = FindParentOfType(sender, typeof(Expander));
                        MainViewModel.FileDeselected(file, element?.DataContext as Bucket);
                    }
                }
            }
        }

        private void FilesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = e?.AddedItems;
            var deselected = e?.RemovedItems;

            foreach (var file in selected) FilesGridView.SelectedItems.Add(file);
            foreach (var file in deselected) FilesGridView.SelectedItems.Remove(file);

            if (selected != null)
            {
                foreach (var i in selected)
                {
                    Debug.WriteLine($"Selected items {i}");
                    if (i is StorageFileWrapper file)
                    {
                        var element = FindParentOfType(sender, typeof(Expander));
                        MainViewModel.FileSelected(file, element?.DataContext as Bucket);
                    }
                }
            }

            if (deselected != null)
            {
                foreach (var i in deselected)
                {
                    Debug.WriteLine($"Deselected items {i}");
                    if (i is StorageFileWrapper file)
                    {
                        var element = FindParentOfType(sender, typeof(Expander));
                        MainViewModel.FileDeselected(file, element?.DataContext as Bucket);
                    }
                }
            }
        }
    }

    internal class FileToThumbnailImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            BitmapImage image = null;

            try
            {
                if (value is StorageFileWrapper fileWrapper)
                {
                    StorageItemThumbnail thumbnail = null;
                    var mimeType = fileWrapper.ContentType;
                    var file = Task.Run(async () => await StorageFile.GetFileFromPathAsync(fileWrapper.Path)).Result;
                    if (mimeType.StartsWith("image/"))
                    {
                        thumbnail = Task.Run(async () => await file.GetScaledImageAsThumbnailAsync(ThumbnailMode.PicturesView, 150)).Result;
                    }
                    else if (mimeType.StartsWith("video/"))
                    {
                        thumbnail = Task.Run(async () => await file.GetScaledImageAsThumbnailAsync(ThumbnailMode.VideosView, 150)).Result;
                    }
                    else if (mimeType.StartsWith("audio/"))
                    {
                        thumbnail = Task.Run(async () => await file.GetScaledImageAsThumbnailAsync(ThumbnailMode.MusicView, 150)).Result;
                    }

                    if (thumbnail == null)
                    {
                        thumbnail = Task.Run(async () => await file.GetScaledImageAsThumbnailAsync(ThumbnailMode.SingleItem, 150)).Result;
                    }
                    image = new BitmapImage();
                    image.SetSource(thumbnail);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return image;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    internal class ClosedDisplayModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is List<Tuple<Bucket, StorageFileWrapper>> selectedList)
            {
                if (selectedList.Count > 0) return AppBarClosedDisplayMode.Compact;
            }
            return AppBarClosedDisplayMode.Minimal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    internal class DebugConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            Debugger.Break();
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            Debugger.Break();
            return value;
        }
    }

    internal class ViewTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is ResultsViewType viewType)
            {
                if (parameter is ResultsViewType test)
                {
                    return viewType == test;
                }
                else if (parameter is string str)
                {
                    test = (ResultsViewType)Enum.Parse(typeof(ResultsViewType), str);
                    return viewType == test;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class ViewTypeStateTrigger : StateTriggerBase
    {
        private ResultsViewType _DesiredViewType;
        public string DesiredViewType
        {
            get => _DesiredViewType.ToString();
            set => _DesiredViewType = (ResultsViewType)Enum.Parse(typeof(ResultsViewType), value);
        }

        public object CurrentViewType
        {
            get => (ResultsViewType)GetValue(CurrentViewTypeProperty);
            set
            {
                SetValue(CurrentViewTypeProperty, value);
                SetActive(_DesiredViewType == (ResultsViewType)value);
            }
        }

        public static readonly DependencyProperty CurrentViewTypeProperty =
            DependencyProperty.RegisterAttached(nameof(CurrentViewType), typeof(object), typeof(ViewTypeStateTrigger), new PropertyMetadata(ResultsViewType.Grid));

    }
}
