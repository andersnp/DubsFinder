﻿using DubsFinder.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace DubsFinder.Models
{
    public class Bucket : Observable
    {
        public string DisplayName
        {
            get
            {
                if (Files.Count > 0) return Files[0].DisplayName;
                else return null;
            }
        }
        public string Hash { get; set; }
        private bool _IsExpanded;
        public bool IsExpanded
        {
            get => _IsExpanded;
            set => Set(ref _IsExpanded, value, nameof(IsExpanded));
        }
        public ObservableCollection<StorageFileWrapper> Files { get; set; }

        private ViewModels.ResultsViewType _ResultsViewType;
        public ViewModels.ResultsViewType ResultsViewType
        {
            get => _ResultsViewType;
            set => Set(ref _ResultsViewType, value, nameof(ResultsViewType));
        }

        public Bucket(StorageFileWrapper file)
        {
            Files = new ObservableCollection<StorageFileWrapper>() { file };
        }

        public void AddFile(StorageFileWrapper file)
        {
            lock (this)
            {
                Files.Add(file);
            }
        }

        public void RemoveFile(StorageFileWrapper file)
        {
            lock (this)
            {
                if (Files.Count == 1 && Files[0] == file) Files = new ObservableCollection<StorageFileWrapper>();
                else Files.Remove(file);
            }
        }
    }
}
