﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;

namespace DubsFinder.Models
{
    internal class Hasher
    {
        private uint BufferCapacity = 100_000_000;
        private Windows.Storage.Streams.Buffer Buffer;

        public Hasher(uint bufferCapacity)
        {
            BufferCapacity = bufferCapacity;
            Buffer = new Windows.Storage.Streams.Buffer(BufferCapacity);
        }

        public async Task<string> HashFileAsync(StorageFile file)
        {
            var algorithm = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha1);
            using (var fileStream = await file.OpenSequentialReadAsync())
            {
                var hash = algorithm.CreateHash();
                while (true)
                {
                    await fileStream.ReadAsync(Buffer, BufferCapacity, Windows.Storage.Streams.InputStreamOptions.ReadAhead);
                    if (Buffer.Length > 0)
                    {
                        hash.Append(Buffer);
                    }
                    else break;
                }

                return CryptographicBuffer.EncodeToHexString(hash.GetValueAndReset()).ToUpper();
            }
        }
    }
}
