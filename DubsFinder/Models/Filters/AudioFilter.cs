﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DubsFinder.Models.Filters
{
    internal class AudioFilter : IFilter
    {
        public bool Filter(Bucket bucket)
        {
            return bucket.Files[0].ContentType.StartsWith("audio/");
        }
    }
}
