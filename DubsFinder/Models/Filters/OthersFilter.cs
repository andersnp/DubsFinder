﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DubsFinder.Models.Filters
{
    internal class OthersFilter : IFilter
    {
        public bool Filter(Bucket bucket)
        {
            var mimeType = bucket.Files[0].ContentType;
            return !mimeType.StartsWith("image/") && !mimeType.StartsWith("audio/") && !mimeType.StartsWith("video/");
        }
    }
}
