﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DubsFinder.Models.Filters
{
    public interface IFilter
    {
        bool Filter(Bucket bucket);
    }
}
