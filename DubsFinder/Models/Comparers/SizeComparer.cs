﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DubsFinder.Models.Comparers
{
    internal class SizeComparer : IComparer<Bucket>, IComparer
    {
        public int Compare(object x, object y)
        {
            if (x is Bucket xb && y is Bucket yb) return Compare(xb, yb);
            return 0;
        }

        public int Compare(Bucket x, Bucket y)
        {
            return x.Files[0].Size.CompareTo(y.Files[0].Size);
        }
    }
}
