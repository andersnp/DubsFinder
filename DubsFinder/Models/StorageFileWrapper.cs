﻿using DubsFinder.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace DubsFinder.Models
{
    public class StorageFileWrapper
    {
        public string Name { get; private set; }

        public string DisplayName { get; private set; }

        public string Path { get; private set; }

        public string ContentType { get; private set; }

        public ulong Size { get; private set; }

        public string DisplayString => $"{Name} ({PrettyFileSizeStringHelper.FileSizeToString(Size)})";

        protected StorageFileWrapper(StorageFile file)
        {
            Name = file?.Name;
            DisplayName = file?.DisplayName;
            Path = file?.Path;
            ContentType = file?.ContentType;
        }

        public static async Task<StorageFileWrapper> CreateStorageFileWrapperAsync(StorageFile file)
        {
            var wrapper = new StorageFileWrapper(file)
            {
                Size = (await file.GetBasicPropertiesAsync()).Size
            };
            return wrapper;
        }

        public async Task<StorageFile> AsStorageFileAsync()
        {
            return await StorageFile.GetFileFromPathAsync(Path);
        }

        public override bool Equals(object obj)
        {
            var wrapper = obj as StorageFileWrapper;
            return wrapper != null &&
                   Name == wrapper.Name &&
                   DisplayName == wrapper.DisplayName &&
                   Path == wrapper.Path &&
                   ContentType == wrapper.ContentType &&
                   Size == wrapper.Size;
        }

        public override int GetHashCode()
        {
            var hashCode = 784610026;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(DisplayName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Path);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ContentType);
            hashCode = hashCode * -1521134295 + Size.GetHashCode();
            return hashCode;
        }
    }
}
