﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DubsFinder.Helpers
{
    internal class PrettyFileSizeStringHelper
    {
        private const double Kilobyte = 1024;
        private const double Megabyte = Kilobyte * 1024;
        private const double Gigabyte = Megabyte * 1024;

        public static string FileSizeToString(ulong fileSize)
        {
            if (fileSize >= Gigabyte) return $"{fileSize / Gigabyte:F1} GB";
            else if (fileSize >= Megabyte) return $"{fileSize / Megabyte:F1} MB";
            else if (fileSize >= Kilobyte) return $"{fileSize / Kilobyte:F1} KB";
            else return $"{fileSize} B";
        }
    }
}
